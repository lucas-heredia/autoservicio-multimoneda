import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  cache: Product[];

  selectedProduct?: Product | null;

  constructor(
    
  ) { 
    this.cache = [];
  }

  getProducts():Observable<Product[]> {

    return new Observable<Product[]>(res => {

      setTimeout(() => {
        let products: Product[] = [
          {
            id: 1,
            image: '../../../assets/images/simple.png',
            name: 'simple',
            description: 'Cucurucho con una bocha de helado, sabor a elección.',
            price: 1,
            currency: 'Dolar Oficial'
          },
          {
            id: 2,
            image: '../../../assets/images/especial.png',
            name: 'especial',
            description: 'Cucurucho con tres bochas de helado, sabor a elección.',
            price: 2,
            currency: 'Dolar Blue'
          },
          {
            id: 3,
            image: '../../../assets/images/bañado.png',
            name: 'bañado',
            description: 'Cucurucho con una bocha de helado y bañado de chocolate, sabor a elección.',
            price: 3,
            currency: 'Dolar Contado con Liqui'
          },
          {
            id: 4,
            image: '../../../assets/images/tresgustos.png',
            name: 'tres gustos',
            description: 'Cucurucho con tres bochas de helado, sabor a elección.',
            price: 4,
            currency: 'Dolar Bolsa'
          }
        ];
        this.cache = products;
        res.next(products);
      }, 1000)
    });
  }

  getCache() {
    return this.cache;
  }

  setSelectedProduct(product: Product) {
    this.selectedProduct = product;
  }

  getSelectedProduct() {
    return this.selectedProduct;
  }

  clearSelectedProduct() {
    this.selectedProduct = null;
  }
}
