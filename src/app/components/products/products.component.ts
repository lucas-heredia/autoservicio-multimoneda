import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: any[];
  isLoading: boolean;
  selectedProduct?: Product;

  constructor(
    private router: Router,
    private location: Location,
    private serviceProducts: ProductsService
  ) {
    this.products = [];
    this.isLoading = false;
   }

  ngOnInit(): void {
    // Check if exists Products cache or call the services
    if (this.serviceProducts.getCache() && this.serviceProducts.getCache().length > 0) {
      this.products = this.serviceProducts.getCache();
    } else {
      this.isLoading = true;
      this.serviceProducts.getProducts().subscribe((res) => {
        this.isLoading = false;
        this.products = res;
      });
    }
  }

  pay() {

    this.serviceProducts.setSelectedProduct(this.selectedProduct!);
    this.router.navigateByUrl('sale');
  }

  cancel() {
    this.serviceProducts.clearSelectedProduct();
    this.location.back();
  }

  selectProduct(product: Product) {
    this.selectedProduct = Object.assign({}, product);
  }
}
