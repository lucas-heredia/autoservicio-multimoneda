import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: any[];
  isLoading: boolean;

  constructor(
    private router: Router,
    private location: Location,
    private serviceCategories: CategoriesService
  ) { 
    this.isLoading = false;
    this.categories = [];
  }

  ngOnInit(): void {
    // Check if exists categories or call the service
    if (this.serviceCategories.getCache() && this.serviceCategories.getCache().length > 0) {
      this.categories = this.serviceCategories.getCache();
    } else {
      this.isLoading = true;
      this.serviceCategories.getCategories().subscribe((res) => {
        this.isLoading = false;
        this.categories = res;
      });
    }
  }

  pay() {
    this.router.navigateByUrl('/products');
  }

  back() {
    this.router.navigateByUrl('');
  }

  selectCategory() {
    this.router.navigateByUrl('/products');
  }
}
