import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  product?: Product;

  constructor(
    private router: Router,
    private productService: ProductsService
  ) {
    this.product = {
      id: 0,
      name: '',
      description: '',
      price: 0,
      currency: '',
      image: ''
    }
   }

  ngOnInit(): void {
    // Check if a product was selected or redirect
    if (this.productService.getSelectedProduct()) {
      this.product = this.productService.getSelectedProduct()!;
    } else {
      this.router.navigateByUrl('products');
    }
  }
}
