import { Component, OnInit } from '@angular/core';
import { SaleService } from './services/sale.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Autoservicio';

  constructor(
    private saleService: SaleService
  ) {

  }
}
