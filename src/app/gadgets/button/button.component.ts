import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() text: string;
  @Input() color: string;
  @Input() type: string;
  @Input() disabled: boolean;

  @Output() clickEvent = new EventEmitter<any>();

  constructor() {
    this.text = '';
    this.color = 'green';
    this.type = 'button';
    this.disabled = false;
  }

  onClick() {
    this.clickEvent.emit();
  }
}
